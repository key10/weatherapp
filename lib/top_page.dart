//画面ファイル
//stful で クラス、StatefulWidget　自動作成
// ------------------------------------------------

import 'package:flutter/material.dart';
import 'package:weather_app02/weather.dart';
import 'package:intl/intl.dart';
import 'package:weather_app02/zip_code.dart';


// ↑ 外部ライブラリの中:  /Users/kyoheiogawa/development/flutter/packages/flutter/lib/material.dart にあるファイル

class TopPage extends StatefulWidget {
  const TopPage({Key? key}) : super(key: key);

  @override
  _TopPageState createState() => _TopPageState();
}

class _TopPageState extends State<TopPage> {


  Weather currentWeather = Weather(
      temp:0,description: '-' ,
      time: DateTime(2021,10,01,14) ,
      lon: 139.6631,lat: 35.6376);

  String adress ="..";   //番号検索で検索した場所格納メンバ
  String errorMessage ="" ;

  //  1時間ごとの天気
  List<Weather>? hourlyWeather=[];
  //  日ごとの天気
  List<Weather>? dailyWeather =[]; //曜日天気

  List<String> weekDay = ["月","火","水","木","金","土","日"];  //曜日初期化

  @override
  Widget build(BuildContext context) {

    // return を変更 ( Container から Scaffold へ )
    return Scaffold(
      body: SafeArea(     //SafeArea == 画面の上部や下部の使わないエリア除外関数
          child : Column(
            children: [
              //郵便番号入力エリア
              //  NullSafetyの為常にfalse、画面にnull文字が入るため三項でハンドリング
              //Error文言
              Text(errorMessage  == "null" ? "" : errorMessage, style: TextStyle(color: Colors.red),),
              SizedBox(height: 30),
              //検索住所
              Text("$adress",style: TextStyle(fontSize: 30),),
              // VerUpによりNull許容、String参照記述変更
              Text(currentWeather.description.toString()),
              //文字列変換方法 ↓ 文字列パラメータとして渡す、 ↑ toString() 使用する。
              Text("${currentWeather.temp}°",style: TextStyle(fontSize: 70),),
              Row(
                //横に並べ デフォ左寄る為、中央よせへ変更
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all( 8.0),
                    child: Text("最高${currentWeather.tempMax}℃ /"),
                  ),
                  Text("最低${currentWeather.tempMin}℃"),
                ],
              ),


              Expanded(
                // flex: 1, //郵便番号入力時高さOverFlowするため指定 2021/09/07
                child: Container(
                    width: 150,
                    margin: EdgeInsets.only(top:  30),
                    child: TextField(
                      onSubmitted: (value) async{
                        print( "----------------------------郵便番号が入力されました = > " +value);
                        //郵便番号から住所検索
                        Map<String, String> response ={};

                        response =  await ZipCode.searchAdressForZipCode(value);
                        errorMessage = response["message"].toString();

                        if(response.containsKey("adress")){
                          adress = response["adress"].toString();
                          print(adress);
                        }
                        // 現在天気
                        currentWeather= await Weather.getCurrentWeather(value);

                        //1時間天気   代入変換に関して　　Video 17 日毎天気情報取得 5:33



                        //2021/09/17　追加　mapで受け取り
                        Map<String , List<Weather>>? weatherForecast = await  Weather.getForecast(lon: currentWeather.lon,  lat: currentWeather.lat);

                        print("top_page Map受け取り　時間、毎日天気情報 ↓");
                        hourlyWeather = weatherForecast?["hourly"];
                        print(hourlyWeather);
                        dailyWeather = weatherForecast?["daily"];
                        print(dailyWeather);
                        print("top_page Map受け取り　時間、毎日天気情報 ↑");

                        // print(" -----1 取得した時間天気 [getHourlyWeather()呼び出し時] ------- Null判定結果 =  await  Weather.getHourlyWeather(currentWeather.lon,currentWeather.lat); 実行時");

                        setState(() { //build ReRun関数
                        });
                      },
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          hintText: "    Postal Code 入力"
                      ),
                    )),
              ),


              SizedBox(height:  7),
              Divider(height: 0,), //1時間ごとの天気 ------------------------ 上線
              SingleChildScrollView(
                // スクロール設定
                //デフォ　上下スクロールのため横スクに変更   == scrollDirection: Axis.horizontal
                scrollDirection: Axis.horizontal,
                child: hourlyWeather == null ? Container(): Row(
                  children: hourlyWeather!.map((weather){   //NUll参照によりエラー ?
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0,vertical: 8.0),   //symmetric == 相対値で設定

                      child: Column(
                        children: [       //-------------------各時間 天気予測エリア 動的に変更箇所 時間、降水確率、アイコン、気温、天気
                          Text("${DateFormat("H").format(weather.time)}時"),
                          // Text("${weather.rainyPercent}%",style: TextStyle(color: Colors.blueAccent),),

                          // todo アクセス通れば下記に変更   ※修正箇所
                          hourlyWeather == null ? Icon(Icons.wb_sunny_outlined) : Image.network("http://openweathermap.org/img/wn/${weather.icon}.png"),
                          Text("${weather.temp}°"),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Text("${weather.description}",style: TextStyle(fontSize: 15),),
                          ),
                        ],
                      ),
                    );
                  }).toList(),
                ),
              ),

              Divider(height: 0,), //------------------------下線

              dailyWeather == null? Container() :
              Expanded(   //Expanded で日別天気エリアのみスクロール可能に変更   ※無いと全体にスクロール設定になるためエラー発生した
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5.0),
                    child:     dailyWeather == null?Container() : Column(
                      children: dailyWeather!.map((weather){

                        return Container(
                          height: 50, //上下余白
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween, //等間隔分け
                            children: [
                              Text("${weekDay[weather.time.weekday -1]}曜日"),
                              //曜日取得関数 weekday : /flutter/bin/cache/pkg/sky_engine/lib/core/date_time.dart
                              dailyWeather == null ? Icon(Icons.wb_sunny_outlined) : Image.network("http://openweathermap.org/img/wn/${weather.icon}.png"),
                              Text("${weather.rainyPercent}%",style: TextStyle(color: Colors.blueAccent),),  //降水確率
                              Row(
                                children: [
                                  Text("${weather.tempMax}° " ,style: TextStyle(color: Colors.redAccent ,fontSize:15 ),),
                                  Text("${weather.tempMin}°",style: TextStyle(color: Colors.indigoAccent ,fontSize:15 ),),
                                ],
                              ),//最高最低気温度
                            ],
                          ),
                        );
                      }).toList(),

                    ),
                  ),
                ),
              )
            ],
          )
      ),
    );
  }
}

//　todo  A RenderFlex overflowed by 17 pixels on the bottom. エラー
// todo 毎日の天気下振れしてるため同時にレイアウト修正
// SingleChildScrollView で包む。
