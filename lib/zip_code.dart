import 'dart:convert';

import 'package:http/http.dart';

class ZipCode{

  static Future<Map<String,String>> searchAdressForZipCode(String zipCode)  async {

    String url = "https://zipcloud.ibsnet.co.jp/api/search?zipcode=$zipCode";
    //動的URL作成箇所      ※User入力したPostCode（7桁）が引数で渡る。

    try{
      //推論型でurlをURIに変換 , jsonデータの内容をマップかたで受け取り、data で初期化、
      var result = await  get(Uri.parse(url));
      Map<String, dynamic> data = jsonDecode(result.body);

      Map<String,String> response = {};
      //エラー時の処理の場合 エラーmessageがある場合
      if(data["message"] != null){
        response["message"] =  "桁数が正しくありません";//data["message"];
      }else{
        // エラー時(結果なしの場合)
        if(data["results"] == null){
          response["message"] = "正しい番号が必要です";
        }else{
          //エラーが無く住所取得できた場合の処理
          //正常作動でもeroorMessageに nullが入り画面出力する為 message を " " で初期化
          // response["message"] = " ";
          response["adress"] = data["results"][0]["address2"];
        }
      }
      print(data);
      return response;

    }catch(e){
      //コンソール出力箇所
      print( "-----↓-------エラー内容");
      print(e);
      print( "-----↑-------エラー内容");

      Map<String,String> response = {};
      response["message"] = "見つかりません..";
      return response;
    }
  }
}



//エラーハンドリング
//
// エラー発生(API) 時 　エラーメッセを返す。   桁数違い、存在しないNumber, 何も入力されない場合     message
//
// 発生してなければclass 通常処理
