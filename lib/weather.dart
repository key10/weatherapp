import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';
import 'package:http/http.dart';

class Weather{
  //取得情報

  int ? temp ;   //気温       NULL許容
  int ? tempMax; //最高気温
  int ? tempMin; //最低気温
  String ? description; //天気の状態
  double ? lon;   // 経度
  double  ? lat;   //緯度
  String ? icon; // 天気アイコン
  DateTime  time ;   //現在の時刻
  int   rainyPercent = 0 ; //降水確率

  static String apiID = "appid=8f2875e045d64bd97ec1701302a957e7&lang=ja&units=metric";

  Weather({   //NullSafety機能の為値初期化必須
    this.temp ,
    this.tempMax ,
    this.tempMin ,
    this.description ="des",
    this.lon =0,
    this.lat =0,
    this.icon ="11d",
    required this.time ,
    this.rainyPercent =0
  });

  static Future<Weather> getCurrentWeather( String zipCode) async{
    String _zipCode = "";
    //154-0011 の様にハイフンがある場合そのまま、無ければ3桁後に - 追加
    if(zipCode.substring(2,4).contains("-")){
      _zipCode = zipCode;
    }else{
      _zipCode = zipCode.substring(0,3)+"-"+ zipCode.substring(3);
    }
    print(_zipCode);
    // APIに対してリクエストパラメータ
    String url = "https://api.openweathermap.org/data/2.5/weather?zip=$_zipCode,JP&$apiID";
    print(url);

    //APIからのReturn , Error時コンソール確認
    try{
      var  resultParm = await get(Uri.parse(url));
      Map<String,dynamic> returnData = jsonDecode(resultParm.body);
      print(returnData);

      Weather currentWeather = Weather(
        time: DateTime.now(),     //reqireの為、使用しない,
        description: returnData["weather"][0]["description"],
        temp:  returnData["main"]["temp"].toInt(),
        tempMax: returnData["main"]["temp_max"].toInt(),
        tempMin:returnData["main"]["temp_min"].toInt(),
        lon:returnData["coord"]["lon"],
        lat:returnData["coord"]["lat"],
      );
      return currentWeather;
    }catch(e){
      print("１時間ごと天気情報取得 APIアクセス時エラー内容");
      print(e);
      Weather currentWeather =Weather(
        time: DateTime.now(),     //reqireの為、使用しない,
        //Null 返せない為  404 で初期化
        description: "$e",
        temp:  404,
        tempMax: 404,
        tempMin:404,

      );
      return currentWeather ;
    }
  }


  //-------------   CurrentWeather End ------------   ------------   ------------   ------------   ------------   ------------   ------------



  // 時間ごと+ 日ごと 天気取得関数に変更   (getHourlyWeatherから  getForecast)
  //  static Future <List<Weather>?> getForecast({double? lon , double? lat}) async{
  static Future <Map<String , List<Weather>>?> getForecast({double? lon , double? lat}) async{

    //戻りの型
    Map<String , List<Weather>> response ={};

    String url = "https://api.openweathermap.org/data/2.5/onecall?lat=$lat&lon=$lon&$apiID&exclude=minutely"; //1時間の天気API (dailyも含む)
    //URLOKぽい debage
    print("getHourlyWeather() 呼び出し時の引数使用URL");
    print(url);


    try{
      var result = await get(Uri.parse(url)); //戻りパラメータ
      // print(" try 中　resultパラメータ");
      // print(result);
      Map<String ,dynamic> data = jsonDecode(result.body);
      print("時間ごと天気情報のjsonDataデータのBODY部分");
      print(data);
      // print("getHourlyWeatherアクセス(jasonデータ取得完了)");
      List<dynamic> hourlyWeatherData = data["hourly"];
      List<dynamic> dailyWeatherData = data["daily"];

      // print("時間ごとの天気データ   ↓");
      List<Weather> hourlyWeather = hourlyWeatherData.map((weather){
        return Weather(
          // 正しくアクセスできてない場合UnixTime : Defaultの 1975年でフォーマットされる。
          temp: weather["temp"].toInt(),
          time: DateTime.fromMillisecondsSinceEpoch(weather["dt"] * 1000),
          icon: weather["weather"][0]["icon"],
          description: weather["weather"][0]["description"],
        );
      }).toList();
      //　　hourlyWeather　内容   確認OK　　パラメータに無い値参照してたエラー
      print("実行テスト hourlyWeather パラメータ");
      print(hourlyWeather[0].time);
      print(hourlyWeather[0].icon);
      print(hourlyWeather[0].temp);
      print(hourlyWeather[0].description);

      //  Iterable dailyWeather = dailyWeatherData.map((weather){



      List<Weather> dailyWeather = dailyWeatherData.map((weather){
        return Weather(
          // 正しくアクセスできてない場合UnixTime : Defaultの 1975年でフォーマットされる。
          time: DateTime.fromMillisecondsSinceEpoch(weather["dt"] * 1000),
          icon: weather["weather"][0]["icon"],
          tempMax: weather["temp"]["max"].toInt(),
          tempMin:weather["temp"]["min"].toInt(),

          //0の時パラメータ存在しない為　LINE家yが無ければ　０　で初期化
          rainyPercent: weather.containsKey("rain") ? weather["rain"].toInt() : 0,
        );}).toList();

      //        現在テスト箇所 dailyWeatherデータの取得に関して
      print("+++++++++++++++++++ 現在テスト箇所 dailyWeatherデータの取得に関して +++++++++++++++++++");
      print(dailyWeather[0].time);
      print(dailyWeather[0].icon);
      print(dailyWeather[0].tempMax);
      print(dailyWeather[0].tempMin);
      print(dailyWeather[0].rainyPercent);

      //時間別天気
      response["hourly"] = hourlyWeather;
      //日別天気
      response["daily"] = dailyWeather;


      //return 抜けの為　Null参照してた箇所
      print("-------responseReturn成功");
      return response;

    }catch(e){
      print("----------getForecast--アクセスError  ↓");
      print(e);

      return null;
    }
  }

}//ClassEnd


// 2021/09/16 現時点まで Masterにプッシュ済み

// Weather   17 - 日毎天気取得、　表示テスト   (動画コメント検索 top_pageの変更箇所怪しい, 受け取り側でListのまま受け取れるのか？　Mapに変更下のに)
// 無理なら　関数追加　記述変更

//世田谷　上馬　の天気 Access : https://api.openweathermap.org/data/2.5/onecall?lat=35.6376&lon=139.6631&appid=8f2875e045d64bd97ec1701302a957e7&lang=ja&units=metric&exclude=minutely
//　　　APIでHourly の次にdaily 取れてる　　　Listで

//    --------getForecast　エラー (catchに入る)　  MapEntry に成功してない
