import 'package:flutter/material.dart';
import 'package:weather_app02/top_page.dart';
import 'package:weather_app02/weather.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // 右上の不要なラッピングラベルぽい不要なパーツactions: falseで削除可能actions: ,
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),

      home: TopPage(),
    );
  }
}

//タスク管理を　下記の方法でメモることできる タスクバーでtodo確認できる書き方
// ※　今回のタスク
//todo 天気クラス作成         仮Comp      ※コンパエラー発生した  NullSafety : Null許容に関して
//tod 現在の天気情報表示    Comp     Nullに関して記述違いはあるものの、?でパラメータNull許可でComp     2021/08/30
//tod 1時間ごとの天気を表示     Comp  : NullSafetyについて...
//tod 毎日の天気を表示     Comp ※UI少し仕様から変更、
//todo 郵便番号検索のUIを作成　    仮 Comp   ※ソフトキーボード使用時、下にレンダリングはみ出る Exception発生 safeAereaの直下Column
// Containerに高さが388に対して 子要素が396 Columnに対して Flex: 2　で対応してerror 解除した
//  ↑  SingleChildScrollView でラップ？ 未解決 (Iphoneでも発症確認「横画面時」)
//  スクロールで逃がす方法模索中   ※時間ごとのみにスクロかけるか、画面全体にかけるか 調査

//tod 郵便番号から住所検索で取得    Comp    zipcloudで郵便番号取得(API), テストツールPostMan使用
//tod　郵便番号検索APIをdart実装   Comp　　　　ZipCode APIでパラメータ住所検索 zip_code.dartで実装 戻り値の確認までOK
//tod　検索時に郵便番号から住所を取得、表示    9月6日(月)実装 メンバ初期化に + setState(() { }); 追加で同期完了     //build ReRun関数
//tod　検索時に入力内容に間違いがあるときエラー表示 トーストぽい表示    Comp　 ※ NullSafety 環境差異のためエラー文　変更
//tod　 現在の天気情報取得       Comp    ※OpenWeatherAPI で取得　、postman でReturn確認OK

//API デフォ USAに為　Parmにja 追加で日本の　zipCode検索可能になる
// Weather  API : api.openweathermap.org/data/2.5/weather?zip={zip code},{country code}&appid={API key}
//取得確認OK   上馬 (-　必須)
//api.openweathermap.org/data/2.5/weather?zip=154-0011,JP&appid=8f2875e045d64bd97ec1701302a957e7

// 日本仕様に変更 (lang=ja) + 摂氏表示 (units=metric)     [ &lang=ja&units=metric ]
//    api.openweathermap.org/data/2.5/weather?zip=154-0011,JP&appid=8f2875e045d64bd97ec1701302a957e7&lang=ja&units=metric

//todo  現在の天気情報をDartから取得      仮Comp? 0が多い場合,Null参照起こしてる

//  所によっては エラー出現 (null 呼び出してるメソッドがある,0代入で街の名前だけ帰ってきてる)
// I/flutter (17177): NoSuchMethodError: The method '[]' was called on null.
// I/flutter (17177): Receiver: null
// I/flutter (17177): Tried calling: [](0)
// I/flutter (17177): 旭川市

//tod　Comp　取得した情報から現在の天気予報を取得 表示OK
//tod 取得した情報から現在の天気を表示

//tod　Comp　1時間ごとの天気を表示     ※OpenWeather APIのHourly (48h)使用
// ※　不明箇所　　　(NullSafeでエラー再現したフェーズ)
//125 return していなかったためNullになってた

//tod   comp  日毎の天気予報を取得
//tod   comp  取得した情報から日毎の天気情報を表示

//-----------RefactorArea
//todo  正しいzipでもエラー出すタイミング出現、0が多い場合（東北や九州に多い） == currentWeatherでのエラーぽい（毎時に関してエラーなし）

// https://www.udemy.com/course/flutter-api/learn/lecture/26578358#overview   学習  API使用天気App
// ① 画面変更に関して ( Libの中でDartファイルのみで変更した)
//  パラメータ「time」は、そのタイプのために「null」の値を持つことはできませんが、暗黙のデフォルト値は「null」です。 NullSaftyでエラー
//解決策 == Optionalに変更か Requiredに指定、 (Dart1系から2系にVerUpのおかげでコンパエラー発生)


